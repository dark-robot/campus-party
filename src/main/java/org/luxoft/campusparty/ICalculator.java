package org.luxoft.campusparty;

public interface ICalculator<T extends Number> {

  T add(T left, T right);

  T subtract(T left, T right);

  T multiply(T left, T right);

  T divide(T left, T right);
}