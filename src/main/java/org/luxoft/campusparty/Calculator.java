package org.luxoft.campusparty;

public class Calculator implements ICalculator<Float> {

  public Float add(Float left, Float right) {
    return left + right;
  }

  public Float subtract(Float left, Float right) {
    return left - right;
  }

  public Float multiply(Float left, Float right) {
    return left * right;
  }

  public Float divide(Float left, Float right) {
    return left / right;
  }
}