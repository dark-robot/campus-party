var assert = require('assert');
var winston = require('winston');

module.exports = function() {

	var left, right, result;

	this.World = require('../support/world').World;

	this.Given(/^the (\d+) and (\d+) numbers$/, function(leftValue, rightValue, callback) {
		left = leftValue;
		right = rightValue;
		callback();
	});

	this.When(/^use the calculator to add them$/, function(callback) {
		result = Number(left) + Number(right);
		callback();
	});

	this.Then(/^the add result is (\d+)$/, function(resultValue, callback) {
		assert.equal(result, resultValue);
		callback();
	});
};