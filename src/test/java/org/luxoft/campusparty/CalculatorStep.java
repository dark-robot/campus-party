package org.luxoft.campusparty;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.assertj.core.api.Assertions.*;

public class CalculatorStep {

  private ICalculator<Float> calculator;
  private float left, right;
  float result;

  public CalculatorStep() {
    calculator = new Calculator();
  }

  @Given("^the (\\d+) and (\\d+) numbers$")
  public void theAndNumbers(int left, int right) throws Throwable {
    this.left = left;
    this.right = right;
  }

  @When("^use the calculator to add them$")
  public void useTheCalculatorToAddThem() throws Throwable {
    result = calculator.add(left, right);
  }

  @Then("^the add result is (\\d+)$")
  public void theAddResultIs(int result) throws Throwable {
    assertThat(this.result).isEqualTo(result);
  }
}